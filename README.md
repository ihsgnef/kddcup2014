## Milestone
- **0.61607** final, approx 45th
- **0.62272** 2014-07-13
- **0.61732** 2014-07-12
- **0.61699** 2014-07-08
- **0.61561** 2014-06-10
- **0.61282** 2014-06-09
- **0.60076** 2014-06-09
- **0.60010** 2014-05-29

## Attention

- **Using "time" when update_files is the recommanded method now** 
- **In all training set, if a project satisfies all requirements, it's exciting**

## Process

- use update_everything.sh + local/remote to update everything

## TODO

- ~~Remove useless data, before 2010-03-15~~
- ~~Cross-validation generator, 70% vs 30%~~
- ~~Small toy data~~
- ~~Make some naive features~~
- ~~Build the Tester~~
- ~~Build the Trainer~~
- ~~Tester is incorrect(?)~~
- ~~Resource file is not correctly passed~~
- ~~Read skleaern documantation, check model~~
- Features
	- ~~teacher information (other projects...)~~
	- ~~vendor information (type, other projects...)~~
	- teacher threshold instead of teacher exciting
- ~~Some kind of feature might be hard to write~~
- TF-IDF weighting
- ~~Maybe PCA before LR?~~
- Naive Bayes on essays
- Gradient Boosting Regression Tree, with depth 5
- Time-rolling training

## File Tree


		├── code
		│   ├── feature_maker
		│	│   ├── feature_bypasser.py
		│	│   ├── update_features.py
		│	│   └── normalizer.py
		│   ├── list_maker.py
		│   ├── file_bypasser.py
		│   ├── tester.py
		│   ├── linear_svc.py
		│   ├── trainer.py
		│   ├── try_regvalue.sh
		│   └── update_files.sh
		├── data
		│   ├── feature_list.csv --------------- unified feature list
		│   ├── test_list.csv
		│   ├── training_list.csv
		│   ├── original	-------------------- feature makers should read from this folder
		│   │   ├── features
		│	│   │   └── normalized
		│   │   ├── donations.csv
		│   │   ├── essays.csv
		│   │   ├── outcomes.csv
		│   │   ├── projects.csv
		│   │   └── resources.csv
		│   ├── test		-------------------- this is for testing, remote and local
		│   │   ├── features
		│	│   │   └── normalized
		│   │   ├── donations.csv
		│   │   ├── essays.csv
		│   │   ├── outcomes.csv
		│   │   ├── predictions.csv
		│   │   ├── projects.csv
		│   │   └── resources.csv
		│   └── training	-------------------- local test: 70% of the test set; remote test: all the test set
		│       ├── features
		│       │   └── normalized
		│       ├── donations.csv
		│       ├── essays.csv
		│       ├── outcomes.csv
		│       ├── projects.csv
		│       └── resources.csv
		└── misc
