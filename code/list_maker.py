import pandas as pd
import numpy as np
import csv
import random
import sys

train_percentage = 0.7
exciting_percentage = 0.094 # 36710/388987

if len(sys.argv) == 1:
	print 'please choose \'remote\' or \'local\''
	sys.exit(0)

file_list = ['projects', 'resources', 'donations', 'essays', 'outcomes']
id_position = {'projects':0, 'resources':1, 'donations':1, 'essays':0, 'outcomes':0}
train_list = set()

p = pd.read_csv('../data/original/projects.csv')
p = p.sort('date_posted')
projectid_list = set(p['projectid'])
projects = np.array(p)
remote_test_index = np.where(projects[:, -1] >= '2014-01-01')[0]
training_index = np.where(projects[:, -1] < '2014-01-01')[0]
remote_test_list = set(projects[remote_test_index, 0])

o = pd.read_csv('../data/original/outcomes.csv')
outcomes = np.array(o)
exciting_index = np.where(outcomes[:, 1] == 't')[0]
unexciting_index = np.where(outcomes[:, 1] == 'f')[0] 

if sys.argv[1] == 'local':
	remote_test_writer = csv.writer(file('../data/test_list.csv', 'wb'))
if sys.argv[1] == 'remote':
	remote_test_writer = csv.writer(file('../data/test_list.csv', 'wb'))
	remote_test_writer.writerow(['projectid'])
	for item in remote_test_list:
		remote_test_writer.writerow([item])
	print sys.argv[1] + ': test', len(remote_test_list)

if sys.argv[1] == 'local':
	local_training_num = int(len(training_index) * train_percentage)
	local_exciting_num = int(local_training_num * exciting_percentage)
	local_training_list = set(random.sample(projects[exciting_index, 0], local_exciting_num))
	print '		 exciting in local training:', len(local_training_list)
	local_training_list |= set(random.sample(projects[unexciting_index, 0], local_training_num - local_exciting_num))
	print sys.argv[1] + ': training ', len(local_training_list)
	local_training_writer = csv.writer(file('../data/training_list.csv', 'wb'))
	local_training_writer.writerow(['projectid'])
	for item in local_training_list:
		local_training_writer.writerow([item])

if sys.argv[1] == 'remote':
	local_training_list = set(projects[training_index, 0])
	print sys.argv[1] +': training ', len(local_training_list)
	local_training_writer = csv.writer(file('../data/training_list.csv', 'wb'))
	local_training_writer.writerow(['projectid'])
	for item in local_training_list:
		local_training_writer.writerow([item])

if sys.argv[1] == 'local':
	local_test_writer = csv.writer(file('../data/test_list.csv', 'wb'))
	local_test_writer.writerow(['projectid'])
	local_test_num = 0
	for item in projects[:, 0]:
		if (item not in local_training_list) and (item not in remote_test_list):
			local_test_num = local_test_num + 1
			local_test_writer.writerow([item])
	print sys.argv[1] + ': test', local_test_num

if sys.argv[1] == 'time':
	local_training_num = int(len(training_index) * train_percentage)
	local_training_list = set()
	local_test_list = set()
	q = pd.read_csv('../data/original/projects.csv')
	q = q.sort('date_posted')
	qq = np.array(q)
	training_index = np.where(qq[:, -1] < '2014-01-01')[0]
	training_set = np.array(qq[training_index])
	cnt = 0
	for row in training_set:
		cnt = cnt + 1
		if (cnt < local_training_num):
			local_training_list.add(row[0])
		else:
			local_test_list.add(row[0])
	print sys.argv[1] + ': training ', len(local_training_list)
	local_training_writer = csv.writer(file('../data/training_list.csv', 'wb'))
	local_training_writer.writerow(['projectid'])
	for item in local_training_list:
		local_training_writer.writerow([item])
	print sys.argv[1] + ': test ', len(local_test_list)
	local_test_writer = csv.writer(file('../data/test_list.csv', 'wb'))
	local_test_writer.writerow(['projectid'])
	for item in local_test_list:
		local_test_writer.writerow([item])
