import pandas as pd
import numpy as np
import sys

target = ''
if len(sys.argv) > 1:
	target = sys.argv[1]

predictions = pd.read_csv('../data/test/predictions'+target+'.csv')
outcomes = pd.read_csv('../data/test/outcomes.csv')

id_set = set(outcomes['projectid'])
p = np.array(predictions)
for row in p:
	if row[0] not in id_set:
		print 'id error'
		sys.exit(0)

id_set = set(predictions['projectid'])
p = np.array(outcomes)
for row in p:
	if row[0] not in id_set:
		print 'id error'
		sys.exit(0)

predictions = predictions.sort('is_exciting', ascending = False)

o = np.array(outcomes)
p = np.array(predictions)

excitingList = [row[0] for row in o if row[1] == 't']
positiveSum = len(excitingList)		#indicate # of exciting in outcomes
negativeSum = len(o) - positiveSum	#indicate # of not exciting in outcomes
excitingSet = set(excitingList)
#print "#exciting:\t", positiveSum
#print "#negative:\t", negativeSum
#print "#all:\t\t",len(o)

height = 0
area = 0

for row in p:
	if row[0] in excitingSet:
		height = height + 1
	else:
		area = area + height

accuracy = area * 1.0 / (positiveSum * negativeSum)
if accuracy < 0.5:
	print 1 - accuracy
else:
	print accuracy
