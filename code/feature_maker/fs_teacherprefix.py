import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_teacherprefix'] = pd.factorize(projects.teacher_prefix)[0]
writer = csv.writer(file('../../data/' + target + '/features/fs_teacherprefix.csv', 'wb'))
writer.writerow(['projectid', 'fs_teacherprefix1', 'fs_teacherprefix2', 'fs_teacherprefix3', 'fs_teacherprefix4'])
#writer.writerow(['projectid', 'fs_teacherprefix'])
for row in np.array(output):
	data = [row[0], int(row[1] == 0), int(row[1] == 1), int(row[1] == 2), int(row[1] == 3)]
	writer.writerow(data)
	#writer.writerow(row)
