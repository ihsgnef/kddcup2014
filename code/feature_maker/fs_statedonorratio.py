import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
donations = pd.read_csv('../../data/' + target + '/donations.csv')

state_donor_num = {}
for row in np.array(donations):
	if not state_donor_num.has_key(row[4]):
		state_donor_num[row[4]] = 0
	state_donor_num[row[4]] = state_donor_num[row[4]] + 1

b = projects.groupby('school_state').size()

writer = csv.writer(file('../../data/' + target + '/features/fs_statedonorratio.csv', 'wb'))
writer.writerow(['projectid', 'fs_statedonorratio'])
for row in np.array(projects):
	writer.writerow([row[0], float(state_donor_num[row[7]]) / b[row[7]]])
