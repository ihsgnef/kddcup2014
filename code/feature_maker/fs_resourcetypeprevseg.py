import pandas as pd
import numpy as np
import csv, sys, math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
p = np.array(projects.sort('date_posted'))

count_back_num = 500
# current best: 500
area_cnt = {}
project_cnt = {}

for i in xrange(len(p)):
	if not area_cnt.has_key(p[i,25]):
		area_cnt[p[i,25]] = 0
	area_cnt[p[i,25]] = area_cnt[p[i,25]] + 1
	if i - count_back_num >= 0:
		area_cnt[p[i - count_back_num,25]] = area_cnt[p[i - count_back_num,25]] - 1
	project_cnt[p[i,0]] = area_cnt[p[i,25]]

# for key in area_cnt.keys():
# 	print key, area_cnt[key]

writer = csv.writer(file('../../data/' + target + '/features/fs_resourcetypeprevseg.csv', 'wb'))
writer.writerow(['projectid', 'fs_resourcetypeprevseg'])
for row in np.array(projects):
	writer.writerow([row[0], project_cnt[row[0]]])
