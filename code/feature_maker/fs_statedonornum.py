import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
donations = pd.read_csv('../../data/' + target + '/donations.csv')

state_donor_num = {}
for row in np.array(donations):
	if not state_donor_num.has_key(row[4]):
		state_donor_num[row[4]] = 0
	state_donor_num[row[4]] = state_donor_num[row[4]] + 1

writer = csv.writer(file('../../data/' + target + '/features/fs_statedonornum.csv', 'wb'))
writer.writerow(['projectid', 'fs_statedonornum'])
for row in np.array(projects):
	writer.writerow([row[0], state_donor_num[row[7]]])
