import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_secondarysubject'] = pd.factorize(projects.secondary_focus_subject)[0]
writer = csv.writer(file('../../data/' + target + '/features/fs_secondarysubject.csv', 'wb'))
data = ['projectid']
for i in xrange(1, 28):
	data.append('fs_secondarysubject' + str(i))
writer.writerow(data)
for row in np.array(output):
	data = [row[0]]
	for i in xrange(1, 28):
		data.append(int(row[1] == (i - 1)))
	writer.writerow(data)
