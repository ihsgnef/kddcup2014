import pandas as pd
import numpy as np
import sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_doubleimpact'] = pd.factorize(projects.eligible_double_your_impact_match)[0]
output.to_csv('../../data/' + target + '/features/fs_doubleimpact.csv', index = False)
