import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
essays = pd.read_csv('../../data/' + target + '/essays.csv')
outcomes = pd.read_csv('../../data/' + target + '/outcomes.csv')
resources = pd.read_csv('../../data/' + target + '/resources.csv')
 
resources = resources.groupby('projectid').size()
resources = resources.sort_index(ascending = False)
output = pd.DataFrame(columns = ['projectid', 'fs_resourcenum'])
output.projectid = resources.index
output.fs_resourcenum = resources.values
output.to_csv('../../data/' + target + '/features/fs_resourcenum.csv', index = False)
