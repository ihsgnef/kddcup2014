import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_secondaryarea'] = pd.factorize(projects.secondary_focus_area)[0]
output.to_csv('../../data/' + target + '/features/fs_secondaryarea.csv', index = False)
writer = csv.writer(file('../../data/' + target + '/features/fs_secondaryarea.csv', 'wb'))
writer.writerow(['projectid', 'fs_secondaryarea1', 'fs_secondaryarea2', 'fs_secondaryarea3', 'fs_secondaryarea4', 'fs_secondaryarea5', 'fs_secondaryarea6', 'fs_secondaryarea7'])
for row in np.array(output):
	data = [row[0], int(row[1] == 0), int(row[1] == 1), int(row[1] == 2), int(row[1] == 3), int(row[1] == 4), int(row[1] == 5), int(row[1] == 6)]
	writer.writerow(data)
