import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/original/projects.csv')
projects = projects.sort('date_posted')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
p = np.array(projects)

exciting = set()
for row in np.array(outcomes):
	if row[1] == 't':
		exciting.add(row[0])

metro_tot = {}
metro_exc = {}
project_prev = {}

print 'fs_charterreadyprevexciting preprocessing'

for row in p:
	if not metro_tot.has_key(row[17]):
		metro_tot[row[17]] = 0
	if not metro_exc.has_key(row[17]):
		metro_exc[row[17]] = 0

	if metro_tot[row[17]] != 0:
		project_prev[row[0]] = float(metro_exc[row[17]]) / metro_tot[row[17]]
	else:
		project_prev[row[0]] = 0

	metro_tot[row[17]] = metro_tot[row[17]] + 1
	if row[0] in exciting:
		metro_exc[row[17]] = metro_exc[row[17]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_charterreadyprevexciting.csv', 'wb'))
writer.writerow(['projectid', 'fs_charterreadyprevexciting'])
projects = pd.read_csv('../../data/' + target + '/projects.csv')

print 'fs_charterreadyprevexciting finishing'

for row in np.array(projects):
	if not project_prev.has_key(row[0]):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], project_prev[row[0]]])
