import numpy as np
import pandas as pd
import csv, sys
import math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
essays = pd.read_csv('../../data/' + target + '/essays.csv')
outcomes = pd.read_csv('../../data/' + target + '/outcomes.csv')
resources = pd.read_csv('../../data/' + target + '/resources.csv')
 
writer = csv.writer(file('../../data/' + target + '/features/fs_itemquantity.csv', 'wb'))
writer.writerow(['projectid', 'fs_itemquantity'])
item = {}

for row in np.array(resources):
	if not item.has_key(str(row[1])):
		item[str(row[1])] = 0
	item[str(row[1])] = item[str(row[1])] + 1

for row in np.array(projects):
	if item.has_key(str(row[0])):
		if math.isnan(item[str(row[0])]):
			writer.writerow([row[0], 0])
		else:
			writer.writerow([row[0], row[29] / item[str(row[0])]])
	else:
		writer.writerow([row[0], 0])
