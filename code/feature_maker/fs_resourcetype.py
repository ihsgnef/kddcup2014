import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
resources = pd.read_csv('../../data/' + target + '/resources.csv')
types = pd.factorize(resources.project_resource_type)[0]
r = np.array(resources)
pv = {}
for i in xrange(0, len(resources)):
	if not pv.has_key(r[i, 1]):
		pv[r[i, 1]] = set()
	pv[r[i, 1]].add(types[i])

writer = csv.writer(file('../../data/' + target + '/features/fs_resourcetype.csv', 'wb'))
data = ['projectid']
for i in xrange(1, 7):
	data.append('fs_resourcetype' + str(i))
writer.writerow(data)
for row in np.array(projects):
	data = [row[0]]
	for i in xrange(1, 7):
		data.append(int((i - 1) in pv[row[0]]))
	writer.writerow(data)
