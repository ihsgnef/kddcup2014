import pandas as pd
import numpy as np
import csv
import random
import sys

flist = pd.read_csv('../../data/feature_list')
feature_list = np.array(flist)

l1 = pd.read_csv('../../data/training_list.csv')
l2 = pd.read_csv('../../data/test_list.csv')
ll1 = np.array(l1)
ll2 = np.array(l2)
local_training_list = set(ll1[:, 0])
local_test_list = set(ll2[:, 0])

if len(sys.argv) > 1:
	feature_name = sys.argv[1]
	if feature_name == 'last':
		flist = np.array(pd.read_csv('../../data/feature_list'))
		feature_name = str(flist[len(flist) - 1, 0])
	f = pd.read_csv('../../data/original/features/' + feature_name + '.csv')
	feature = np.array(f)
	print 'pass ', feature_name

	training_writer = csv.writer(file('../../data/training/features/' + feature_name + '.csv', 'wb'))
	test_writer = csv.writer(file('../../data/test/features/' + feature_name + '.csv', 'wb'))
	training_writer.writerow(list(f.columns.values))
	test_writer.writerow(list(f.columns.values))
	
	for row in feature:
		if row[0] in local_training_list:
			training_writer.writerow(row)
		if row[0] in local_test_list:
			test_writer.writerow(row)
	sys.exit(0)

for row in feature_list:
	feature_name = row[0]
	
	f = pd.read_csv('../../data/original/features/' + feature_name + '.csv')
	feature = np.array(f)
	print 'use ', feature_name

	training_writer = csv.writer(file('../../data/training/features/' + feature_name + '.csv', 'wb'))
	test_writer = csv.writer(file('../../data/test/features/' + feature_name + '.csv', 'wb'))
	training_writer.writerow(list(f.columns.values))
	test_writer.writerow(list(f.columns.values))
	
	for row in feature:
		if row[0] in local_training_list:
			training_writer.writerow(row)
		if row[0] in local_test_list:
			test_writer.writerow(row)
