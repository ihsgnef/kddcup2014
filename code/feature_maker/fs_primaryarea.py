import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_primaryarea'] = pd.factorize(projects.primary_focus_area)[0]
writer = csv.writer(file('../../data/' + target + '/features/fs_primaryarea.csv', 'wb'))
writer.writerow(['projectid', 'fs_primaryarea1', 'fs_primaryarea2', 'fs_primaryarea3', 'fs_primaryarea4', 'fs_primaryarea5', 'fs_primaryarea6', 'fs_primaryarea7'])
for row in np.array(output):
	data = [row[0], int(row[1] == 0), int(row[1] == 1), int(row[1] == 2), int(row[1] == 3), int(row[1] == 4), int(row[1] == 5), int(row[1] == 6)]
	writer.writerow(data)
