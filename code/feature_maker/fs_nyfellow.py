import pandas as pd
import numpy as np
import sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_nyfellow'] = pd.factorize(projects.teacher_ny_teaching_fellow)[0]
output.to_csv('../../data/' + target + '/features/fs_nyfellow.csv', index = False)
