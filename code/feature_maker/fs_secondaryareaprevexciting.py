import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/original/projects.csv')
projects = projects.sort('date_posted')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
p = np.array(projects)

exciting = set()
for row in np.array(outcomes):
	if row[1] == 't':
		exciting.add(row[0])
teacher_tot = {}
teacher_exc = {}
project_prev = {}

print 'fs_secondaryareaprevexciting preprocessing'

for row in p:
	if not teacher_tot.has_key(row[24]):
		teacher_tot[row[24]] = 0
	if not teacher_exc.has_key(row[24]):
		teacher_exc[row[24]] = 0

	if not teacher_tot.has_key(row[22]):
		teacher_tot[row[22]] = 0
	if not teacher_exc.has_key(row[22]):
		teacher_exc[row[22]] = 0

	if teacher_tot[row[24]] != 0:
		project_prev[row[0]] = float(teacher_exc[row[24]]) / teacher_tot[row[24]]
	else:
		project_prev[row[0]] = 0

	teacher_tot[row[24]] = teacher_tot[row[24]] + 1
	if row[0] in exciting:
		teacher_exc[row[24]] = teacher_exc[row[24]] + 1
	teacher_tot[row[22]] = teacher_tot[row[22]] + 1
	if row[0] in exciting:
		teacher_exc[row[22]] = teacher_exc[row[22]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_secondaryareaprevexciting.csv', 'wb'))
writer.writerow(['projectid', 'fs_secondaryareaprevexciting'])
projects = pd.read_csv('../../data/' + target + '/projects.csv')

print 'fs_secondaryareaprevexciting finishing'

for row in np.array(projects):
	if not project_prev.has_key(row[0]):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], project_prev[row[0]]])
