import pandas as pd
import numpy as np
import csv, sys, math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
p = np.array(projects.sort('date_posted'))

count_back_num = 10
# current best: 2
subject_cnt = {}
project_cnt = {}

for i in xrange(len(p)):
	if not subject_cnt.has_key(p[i,21]):
		subject_cnt[p[i,21]] = 0
	subject_cnt[p[i,21]] = subject_cnt[p[i,21]] + 1
	if i - count_back_num >= 0:
		subject_cnt[p[i - count_back_num,21]] = subject_cnt[p[i - count_back_num,21]] - 1
	project_cnt[p[i,0]] = subject_cnt[p[i,21]]

# for key in subject_cnt.keys():
# 	print key, subject_cnt[key]

writer = csv.writer(file('../../data/' + target + '/features/fs_primarysubjectprevseg.csv', 'wb'))
writer.writerow(['projectid', 'fs_primarysubjectprevseg'])
for row in np.array(projects):
	writer.writerow([row[0], project_cnt[row[0]]])
