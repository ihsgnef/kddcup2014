import numpy as np
import pandas as pd
import csv, sys, math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/original/projects.csv')
projects = projects.sort('date_posted')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
p = np.array(projects)

exciting = set()
for row in np.array(outcomes):
	if row[1] == 't':
		exciting.add(row[0])

metro_tot = {}
metro_exc = {}
project_prev = {}

print 'fs_studentreachedprevexciting preprocessing'

for row in p:
	if math.isnan(row[31]):
		row[31] = 0
	else:
		row[31] = int(row[31] / 10)
	if not metro_tot.has_key(row[31]):
		metro_tot[row[31]] = 0
	if not metro_exc.has_key(row[31]):
		metro_exc[row[31]] = 0

	if metro_tot[row[31]] != 0:
		project_prev[row[0]] = float(metro_exc[row[31]]) / metro_tot[row[31]]
	else:
		project_prev[row[0]] = 0

	metro_tot[row[31]] = metro_tot[row[31]] + 1
	if row[0] in exciting:
		metro_exc[row[31]] = metro_exc[row[31]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_studentreachedprevexciting.csv', 'wb'))
writer.writerow(['projectid', 'fs_studentreachedprevexciting'])
projects = pd.read_csv('../../data/' + target + '/projects.csv')

print 'fs_studentreachedprevexciting finishing'

for row in np.array(projects):
	if not project_prev.has_key(row[0]):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], project_prev[row[0]]])
