import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_schoolmetro'] = pd.factorize(projects.school_metro)[0]
writer = csv.writer(file('../../data/' + target + '/features/fs_schoolmetro.csv', 'wb'))
writer.writerow(['projectid', 'fs_schoolmetro1', 'fs_schoolmetro2', 'fs_schoolmetro3'])
for row in np.array(output):
	data = [row[0], int(row[1] == 0), int(row[1] == 1), int(row[1] == 2)]
	writer.writerow(data)
