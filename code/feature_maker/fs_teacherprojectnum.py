import pandas as pd
import numpy as np
import csv, sys, math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]

projects = pd.read_csv('../../data/' + target + '/projects.csv')
p = projects.sort('date_posted')
teacher_cnt = {}
project_prev_num = {}


for row in np.array(p):
	if not teacher_cnt.has_key(row[1]):
		teacher_cnt[row[1]] = 0
	project_prev_num[row[0]] = teacher_cnt[row[1]]
	teacher_cnt[row[1]] = teacher_cnt[row[1]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_teacherprojectnum.csv', 'wb'))
writer.writerow(['projectid', 'fs_teacherprojectnum'])
for row in np.array(projects):
	if math.isnan(project_prev_num[row[0]]):
		project_prev_num[row[0]] = 0
	writer.writerow([row[0], project_prev_num[row[0]]])
