import numpy as np
import pandas as pd
import csv, sys
import math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_priceexclude'] = projects['total_price_excluding_optional_support'] / 10
output = np.array(output)
writer = csv.writer(file('../../data/' + target + '/features/fs_priceexclude.csv', 'wb'))
writer.writerow(['projectid', 'fs_priceexclude'])
max_price = 5000
for row in output:
	if math.isnan(row[1]):
		row[1] = 0
	else:
		row[1] = int(row[1])

for row in output:
	#writer.writerow([row[0], 1 if row[1] > max_price else float(row[1]) / max_price])
	writer.writerow(row)
	

