import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/original/projects.csv')
projects = projects.sort('date_posted')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
p = np.array(projects)

exciting = set()
for row in np.array(outcomes):
	if row[1] == 't':
		exciting.add(row[0])

nlns_tot = {}
nlns_exc = {}
project_prev = {}

print 'fs_schoolnlnsprevexciting preprocessing'

for row in p:
	if not nlns_tot.has_key(row[15]):
		nlns_tot[row[15]] = 0
	if not nlns_exc.has_key(row[15]):
		nlns_exc[row[15]] = 0

	if nlns_tot[row[15]] != 0:
		project_prev[row[0]] = float(nlns_exc[row[15]]) / nlns_tot[row[15]]
	else:
		project_prev[row[0]] = 0

	nlns_tot[row[15]] = nlns_tot[row[15]] + 1
	if row[0] in exciting:
		nlns_exc[row[15]] = nlns_exc[row[15]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_schoolnlnsprevexciting.csv', 'wb'))
writer.writerow(['projectid', 'fs_schoolnlnsprevexciting'])
projects = pd.read_csv('../../data/' + target + '/projects.csv')

print 'fs_schoolnlnsprevexciting finishing'

for row in np.array(projects):
	if not project_prev.has_key(row[0]):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], project_prev[row[0]]])
