import pandas as pd
import numpy as np
import csv, sys
import datetime

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')

writer = csv.writer(file('../../data/' + target + '/features/fs_month.csv', 'wb'))
# writer.writerwo(['projectid','fs_month1','fs_month2','fs_month3','fs_month4','fs_month5','fs_month6','fs_month7','fs_month8','fs_month9','fs_month10','fs_month11','fs_month12'])
writer.writerow(['projectid', 'fs_month'])

for row in np.array(projects):
	day = row[-1]
	d = int(day[5:7])
	writer.writerow([row[0], d])
'''
writer = csv.writer(file('../../data/' + target + '/features/fs_month.csv', 'wb'))
writer.writerow(['projectid','fs_month1','fs_month2','fs_month3','fs_month4','fs_month5','fs_month6','fs_month7','fs_month8','fs_month9','fs_month10','fs_month11','fs_month12'])
for row in np.array(projects):
	day = row[-1]
	d = int(day[5:7])
	writer.writerow([row[0],int(d==0),int(d==1),int(d==2),int(d==3),int(d==4),int(d==5),int(d==6),int(d==7),int(d==8),int(d==9),int(d==10),int(d==11),int(d==12)])
'''
