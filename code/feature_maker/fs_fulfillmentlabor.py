import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_fulfillmentlabor'] = pd.factorize(projects.fulfillment_labor_materials)[0]
writer = csv.writer(file('../../data/' + target + '/features/fs_fulfillmentlabor.csv', 'wb'))
writer.writerow(['projectid', 'fs_fulfillmentlabor1', 'fs_fulfillmentlabor2', 'fs_fulfillmentlabor3'])
for row in np.array(output):
	data = [row[0], int(row[1] == 0), int(row[1] == 1), int(row[1] == 2)]
	writer.writerow(data)
