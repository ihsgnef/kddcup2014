import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
p = pd.read_csv('../../data/' + target + '/projects.csv')
e = pd.read_csv('../../data/' + target + '/essays.csv')
 
essay_length = {}
sentence_number = {}
essays = np.array(e)
projects = np.array(p)

#projectid,teacher_acctid,title,short_description,need_statement,essay
for row in essays:
	essay_length[row[0]] = len(str(row[5]))
	sentence_number[row[0]] = str(row[5]).count('.')
	if sentence_number[row[0]] == 0:
		sentence_number[row[0]] = 1

writer = csv.writer(file('../../data/' + target + '/features/fs_sentencelength.csv', 'wb'))
writer.writerow(['projectid', 'fs_sentencelength'])

for row in projects:
	if row[0] not in essay_length:
		print 'error'
	else:
		data = [row[0], float(essay_length[row[0]]) / sentence_number[row[0]]]
		writer.writerow(data)
