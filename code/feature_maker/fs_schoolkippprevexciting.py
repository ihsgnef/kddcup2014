import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/original/projects.csv')
projects = projects.sort('date_posted')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
p = np.array(projects)

exciting = set()
for row in np.array(outcomes):
	if row[1] == 't':
		exciting.add(row[0])

kipp_tot = {}
kipp_exc = {}
project_prev = {}

print 'fs_schoolkippprevexciting preprocessing'

for row in p:
	if not kipp_tot.has_key(row[16]):
		kipp_tot[row[16]] = 0
	if not kipp_exc.has_key(row[16]):
		kipp_exc[row[16]] = 0

	if kipp_tot[row[16]] != 0:
		project_prev[row[0]] = float(kipp_exc[row[16]]) / kipp_tot[row[16]]
	else:
		project_prev[row[0]] = 0

	kipp_tot[row[16]] = kipp_tot[row[16]] + 1
	if row[0] in exciting:
		kipp_exc[row[16]] = kipp_exc[row[16]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_schoolkippprevexciting.csv', 'wb'))
writer.writerow(['projectid', 'fs_schoolkippprevexciting'])
projects = pd.read_csv('../../data/' + target + '/projects.csv')

print 'fs_schoolkippprevexciting finishing'

for row in np.array(projects):
	if not project_prev.has_key(row[0]):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], project_prev[row[0]]])
