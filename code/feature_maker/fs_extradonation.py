import numpy as np
import pandas as pd
import csv, sys
import math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_extradonation'] = (projects['total_price_including_optional_support'] - projects['total_price_excluding_optional_support']) / projects['total_price_including_optional_support']
output = np.array(output)
writer = csv.writer(file('../../data/' + target + '/features/fs_extradonation.csv', 'wb'))
writer.writerow(['projectid', 'fs_extradonation'])

min_ratio = 0.129955
max_ratio = 0.18

for row in output:
	if math.isnan(row[1]):
		row[1] = 0

for row in output:
	#row[1] = (row[1] - min_ratio) / (max_ratio - min_ratio)
	writer.writerow(row)
	

