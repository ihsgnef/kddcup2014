import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
p = pd.read_csv('../../data/' + target + '/projects.csv')
e = pd.read_csv('../../data/' + target + '/essays.csv')
 
essay_length = {}
essays = np.array(e)
projects = np.array(p)

#projectid,teacher_acctid,title,short_description,need_statement,essay
max_length = 900
for row in essays:
	essay_length[row[0]] = len(str(row[4]))

# print essay_length

writer = csv.writer(file('../../data/' + target + '/features/fs_needstmtlength.csv', 'wb'))
writer.writerow(['projectid', 'fs_needstmtlength'])

for row in projects:
	if row[0] not in essay_length:
		print 'error'
	#data = [row[0], 1 if essay_length[row[0]] > max_length else float(essay_length[row[0]]) / max_length]
	data = [row[0], essay_length[row[0]]]
	writer.writerow(data)

