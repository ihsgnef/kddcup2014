import numpy as np
import pandas as pd
import csv, sys
import math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
essays = pd.read_csv('../../data/' + target + '/essays.csv')
outcomes = pd.read_csv('../../data/' + target + '/outcomes.csv')
resources = pd.read_csv('../../data/' + target + '/resources.csv')
 
writer = csv.writer(file('../../data/' + target + '/features/fs_itemmaxprice.csv', 'wb'))
writer.writerow(['projectid', 'fs_itemmaxprice'])
item = {}

for row in np.array(resources):
	if not item.has_key(str(row[1])):
		item[str(row[1])] = row[-2]
	else:
		if row[-2] > item[str(row[1])]:
			item[str(row[1])] = row[-2]

for row in np.array(projects):
	if item.has_key(str(row[0])):
		if math.isnan(item[str(row[0])]):
			writer.writerow([row[0], 0])
		else:
			writer.writerow([row[0], item[row[0]]])
	else:
		writer.writerow([row[0], 0])
