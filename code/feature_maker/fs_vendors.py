import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
resources = pd.read_csv('../../data/' + target + '/resources.csv')
vendors = pd.factorize(resources.vendorid)[0]
r = np.array(resources)
pv = {}
for i in xrange(0, len(resources)):
	if not pv.has_key(r[i, 1]):
		pv[r[i, 1]] = set()
	pv[r[i, 1]].add(vendors[i])

writer = csv.writer(file('../../data/' + target + '/features/fs_vendors.csv', 'wb'))
data = ['projectid']
for i in xrange(1, 33):
	data.append('fs_vendors' + str(i))
writer.writerow(data)
for row in np.array(projects):
	data = [row[0]]
	for i in xrange(1, 33):
		data.append(int((i - 1) in pv[row[0]]))
	writer.writerow(data)
