import pandas as pd
import numpy as np
import csv, sys
import math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
output = pd.DataFrame()
output['projectid'] = projects['projectid']
output['fs_gradelevel'] = pd.factorize(projects.grade_level)[0]
writer = csv.writer(file('../../data/' + target + '/features/fs_gradelevel.csv', 'wb'))
'''
writer.writerow(['projectid', 'fs_gradelevel1', 'fs_gradelevel2', 'fs_gradelevel3', 'fs_gradelevel4'])
for row in np.array(output):
	data = [row[0], int(row[1] == 0), int(row[1] == 1), int(row[1] == 2), int(row[1] == 3)]
	writer.writerow(data)
'''
grade_level = {'Grades 9-12':3, 'Grades 6-8':2, 'Grades 3-5':1, 'Grades PreK-2':0}
writer.writerow(['projectid', 'fs_gradelevel'])

for row in np.array(projects):
	if not grade_level.has_key(row[27]):
		writer.writerow([row[0], 0])
	else: 
		writer.writerow([row[0], grade_level[row[27]]])
