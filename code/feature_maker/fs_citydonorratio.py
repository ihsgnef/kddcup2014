import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
donations = pd.read_csv('../../data/' + target + '/donations.csv')

state_donor_num = {}
for row in np.array(donations):
	if not state_donor_num.has_key(row[3]):
		state_donor_num[row[3]] = 0
	state_donor_num[row[3]] = state_donor_num[row[3]] + 1

b = projects.groupby('school_city').size()

writer = csv.writer(file('../../data/' + target + '/features/fs_citydonorratio.csv', 'wb'))
writer.writerow(['projectid', 'fs_citydonorratio'])
for row in np.array(projects):
	if not (state_donor_num.has_key(row[6])):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], float(state_donor_num[row[6]]) / b[row[6]]])
