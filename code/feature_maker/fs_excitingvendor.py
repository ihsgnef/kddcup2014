import numpy as np
import pandas as pd
import sys
import csv
import pickle

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]

# resourceid,projectid,vendorid,vendor_name,project_resource_type,item_name,item_number,item_unit_price,item_quantity

if target != 'test':
	# preprocessing
	print 'fs_excitingvendor preprocessing'
	outcomes = pd.read_csv('../../data/' + target + '/outcomes.csv')
	exciting_set = set(np.array(outcomes['projectid'])[np.where(outcomes['is_exciting'] == 't')[0]])

	resources = pd.read_csv('../../data/' + target + '/resources.csv')
	resources = resources.fillna(-1)
	vendor_total = {}
	vendor_exciting = {}
	vendor_perc = {}

	for row in np.array(resources):
		# print row[1], row[2]
		if row[2] != -1:
			if row[1] in exciting_set:
				if not vendor_exciting.has_key(row[2]):
					vendor_exciting[row[2]] = 0
				vendor_exciting[row[2]] = vendor_exciting[row[2]] + 1

			if not vendor_total.has_key(row[2]):
				vendor_total[row[2]] = 0
			vendor_total[row[2]] = vendor_total[row[2]] + 1
	
	for key, val in vendor_total.items():
		if vendor_exciting.has_key(key):
			vendor_perc[key] = float(vendor_exciting[key]) / val
		else:
			vendor_perc[key] = 0
	
	vfile = open("vendor_perc.csv", "w")
	writer = csv.writer(vfile)
	for key, val in vendor_perc.items():
		writer.writerow([key, val])
	vfile.close()
	

# midprrocessing
# resourceid,projectid,vendorid
print 'fs_excitingvendor midprocessing'
vendor_perc = {}
for key, val in csv.reader(open("vendor_perc.csv")):
	vendor_perc[key] = val
project_vendor = {}
resources = pd.read_csv('../../data/' + target + '/resources.csv')
resources = resources.fillna(-1)
for row in np.array(resources):
	if not project_vendor.has_key(row[1]):
		project_vendor[row[1]] = 0
	if row[2] != -1:
		if vendor_perc.has_key(str(row[2])):
			project_vendor[row[1]] = max(project_vendor[row[1]], vendor_perc[str(row[2])])


# finishing
print 'fs_excitingvendor finishing'
projects = pd.read_csv('../../data/' + target + '/projects.csv')
writer = csv.writer(file('../../data/' + target + '/features/fs_excitingvendor.csv', 'wb'))
writer.writerow(['projectid', 'fs_excitingvendor'])

for row in np.array(projects):
	if project_vendor.has_key(str(row[0])):
		writer.writerow([row[0], project_vendor[str(row[0])]])
	else:
		writer.writerow([row[0], 0])
