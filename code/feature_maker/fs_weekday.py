'''
import pandas as pd
import numpy as np
import csv, sys
import datetime

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')

writer = csv.writer(file('../../data/' + target + '/features/fs_weekday.csv', 'wb'))
writer.writerow(['projectid','fs_weekday1','fs_weekday2','fs_weekday3','fs_weekday4','fs_weekday5','fs_weekday6','fs_weekday7'])
for row in np.array(projects):
	day = row[-1]
	year, mon, day = int(day[:4]), int(day[5:7]), int(day[8:])
	d = datetime.datetime(year, mon, day).weekday()
	writer.writerow([row[0], int(d==0),int(d==1),int(d==2),int(d==3),int(d==4),int(d==5),int(d==6)])

'''

import pandas as pd
import numpy as np
import csv, sys
import datetime

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')

writer = csv.writer(file('../../data/' + target + '/features/fs_weekday.csv', 'wb'))
writer.writerow(['projectid', 'fs_weekday'])
for row in np.array(projects):
	day = row[-1]
	year, mon, day = int(day[:4]), int(day[5:7]), int(day[8:])
	d = datetime.datetime(year, mon, day).weekday()
	writer.writerow([row[0], d])

