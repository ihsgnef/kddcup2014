import pandas as pd
import numpy as np
import csv, sys, math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
p = np.array(projects.sort('date_posted'))

count_back_num = 2
# current best: 2
area_cnt = {}
project_cnt = {}

for i in xrange(len(p)):
	if not area_cnt.has_key(p[i,22]):
		area_cnt[p[i,22]] = 0
	area_cnt[p[i,22]] = area_cnt[p[i,22]] + 1
	if i - count_back_num >= 0:
		area_cnt[p[i - count_back_num,22]] = area_cnt[p[i - count_back_num,22]] - 1
	project_cnt[p[i,0]] = area_cnt[p[i,22]]

# for key in area_cnt.keys():
# 	print key, area_cnt[key]

writer = csv.writer(file('../../data/' + target + '/features/fs_primaryareaprevseg.csv', 'wb'))
writer.writerow(['projectid', 'fs_primaryareaprevseg'])
for row in np.array(projects):
	writer.writerow([row[0], project_cnt[row[0]]])
