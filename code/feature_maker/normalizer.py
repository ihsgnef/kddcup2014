import pandas as pd
import numpy as np
import csv
import sys
import math
import os

def dont(feature_name):
	os.system('cp ../../data/training/features/' + feature_name + '.csv ../../data/training/features/normalized/')
	os.system('cp ../../data/test/features/' + feature_name + '.csv ../../data/test/features/normalized/')

def linear(feature_name):
	feature_file = pd.read_csv('../../data/training/features/' + feature_name + '.csv')
	projects = pd.read_csv('../../data/training/projects.csv')
	feature_file = feature_file.sort(feature_name)
	ff = np.array(feature_file)
	rank = {}
	i = 0
	for row in ff:
		i = i + 1
		rank[row[0]] = i
	writer = csv.writer(file('../../data/training/features/normalized/' + feature_name + '.csv', 'wb'))
	writer.writerow(['projectid', feature_name])
	for row in np.array(projects):
		writer.writerow([row[0], float(rank[row[0]]) / len(projects)])

	feature_file = pd.read_csv('../../data/test/features/' + feature_name + '.csv')
	projects = pd.read_csv('../../data/test/projects.csv')
	feature_file = feature_file.sort(feature_name)
	ff = np.array(feature_file)
	rank = {}
	i = 0
	for row in ff:
		i = i + 1
		rank[row[0]] = i
	writer = csv.writer(file('../../data/test/features/normalized/' + feature_name + '.csv', 'wb'))
	writer.writerow(['projectid', feature_name])
	for row in np.array(projects):
		writer.writerow([row[0], float(rank[row[0]]) / len(projects)])


def normal(feature_name):
	feature_file = pd.read_csv('../../data/training/features/' + feature_name + '.csv')
	ff = np.array(feature_file[feature_name])
	writer = csv.writer(file('../../data/training/features/normalized/' + feature_name + '.csv', 'wb'))
	writer.writerow(['projectid', feature_name])
	mean = np.mean(ff)
	std = np.std(ff)
	for row in np.array(feature_file):
		row[1] = (float(row[1]) - mean) / std
		writer.writerow(row)

	feature_file = pd.read_csv('../../data/test/features/' + feature_name + '.csv')
	ff = np.array(feature_file[feature_name])
	writer = csv.writer(file('../../data/test/features/normalized/' + feature_name + '.csv', 'wb'))
	writer.writerow(['projectid', feature_name])
	mean = np.mean(ff)
	std = np.std(ff)
	for row in np.array(feature_file):
		row[1] = (float(row[1]) - mean) / std
		writer.writerow(row)

flist = pd.read_csv('../../data/complete_feature_list')
feature_list = np.array(flist)

if len(sys.argv) > 1:
	feature_name = sys.argv[1]
	for row in feature_list:
		if row[0] == feature_name:
			if row[2] != 'dont':
				print row[0], ': ', row[2]
			exec row[2] + '("' + row[0] + '")'
			break

else :
	print '########## normalize ##########'
	#feature_name,global,normalize,use
	for row in feature_list:
		if row[3] == True:
			if row[2] != 'dont':
				print row[0], ': ', row[2]
			exec row[2] + '("' + row[0] + '")'
