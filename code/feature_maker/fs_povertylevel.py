import pandas as pd
import numpy as np
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
poverty_level = {'highest poverty':3, 'high poverty':2, 'moderate poverty':1, 'low poverty':0}
output = pd.DataFrame()
output['projectid'] = projects['projectid']
#output['fs_povertylevel'] = pd.factorize(projects.poverty_level)[0]
writer = csv.writer(file('../../data/' + target + '/features/fs_povertylevel.csv', 'wb'))
'''
writer.writerow(['projectid', 'fs_povertylevel1', 'fs_povertylevel2', 'fs_povertylevel3', 'fs_povertylevel4'])
for row in np.array(output):
	data = [row[0], int(row[1] == 0), int(row[1] == 1), int(row[1] == 2), int(row[1] == 3)]
	writer.writerow(data)
'''
writer.writerow(['projectid', 'fs_povertylevel'])
for row in np.array(projects):
	writer.writerow([row[0], poverty_level[row[26]]])
