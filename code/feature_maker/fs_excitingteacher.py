import numpy as np
import pandas as pd
import sys
import csv

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]

if target != 'test':
	# preprocessing
	print 'fs_excitingteacher preprocessing' 
	projects = pd.read_csv('../../data/' + target + '/projects.csv')
	outcomes = pd.read_csv('../../data/' + target + '/outcomes.csv')
	
	train_index = np.where(projects['date_posted'] < '2014-01-01')[0]
	test_index = np.where(projects['date_posted'] >= '2014-01-01')[0]
	exciting_index = np.where(outcomes['is_exciting'] == 't')
	train_set = set(np.array(projects['projectid'])[train_index])
	test_set = set(np.array(projects['projectid'])[test_index])
	exciting_set = set(np.array(outcomes['projectid'])[exciting_index])
	
	header = ['projectid', 'teacher_acctid', 'schoolid', 'school_ncesid', 'school_latitude', 'school_longitude', 'school_city', 'school_state', 'school_zip', 'school_metro', 'school_district', 'school_county', 'school_charter', 'school_magnet', 'school_year_round', 'school_nlns', 'school_kipp', 'school_charter_ready_promise', 'teacher_prefix', 'teacher_teach_for_america', 'teacher_ny_teaching_fellow', 'primary_focus_subject', 'primary_focus_area', 'secondary_focus_subject', 'secondary_focus_area', 'resource_type', 'poverty_level', 'grade_level', 'fulfillment_labor_materials', 'total_price_excluding_optional_support', 'total_price_including_optional_support', 'students_reached', 'eligible_double_your_impact_match', 'eligible_almost_home_match', 'date_posted']
	
	p = np.array(projects)
	tr = p[train_index]
	train_writer = csv.writer(file('train_projects.csv', 'wb'))
	train_writer.writerow(header)
	exciting_writer = csv.writer(file('exciting_projects.csv', 'wb'))
	exciting_writer.writerow(header)
	for row in tr:
		train_writer.writerow(row)
		if row[0] in exciting_set:
			exciting_writer.writerow(row)

# midprocessing
print 'fs_excitingteacher midprocessing' 
train_projects = pd.read_csv('train_projects.csv')
exciting_projects = pd.read_csv('exciting_projects.csv')

tt = pd.DataFrame(train_projects.groupby('teacher_acctid').size(), columns = ['all'])
ee = pd.DataFrame(exciting_projects.groupby('teacher_acctid').size(), columns = ['exc'])
tt.to_csv('teacher_all.csv')
ee.to_csv('teacher_exc.csv')

teacher_all = pd.read_csv('teacher_all.csv')
teacher_exc = pd.read_csv('teacher_exc.csv')
output = pd.merge(teacher_all, teacher_exc, on = 'teacher_acctid')
output['perc'] = output['exc'] / output['all']
output.to_csv('teacher_info.csv', index = False)

# making
print 'fs_excitingteacher finishing' 
teacher_info = pd.read_csv('teacher_info.csv')
output = pd.DataFrame()
teacher_dict = {}
exciting_dict = {}
projects = pd.read_csv('../../data/' + target + '/projects.csv')
p = np.array(projects)
for row in p:
	teacher_dict[row[0]] = row[1]
ti = np.array(teacher_info)
for row in ti:
	exciting_dict[row[0]] = row[3]
exciting_set = set(ti[:, 0])

feature_writer = csv.writer(file('../../data/' + target + '/features/fs_excitingteacher.csv', 'wb'))
feature_writer.writerow(['projectid', 'fs_excitingteacher'])
for row in np.array(projects):
	data = [row[0], 0]
	if row[1] in exciting_set:
		data = [row[0], exciting_dict[row[1]]]
	feature_writer.writerow(data)
