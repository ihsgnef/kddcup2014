import pandas as pd
import numpy as np
import csv,sys,math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]

projects = pd.read_csv('../../data/' + target + '/projects.csv')
day = projects.groupby('date_posted').size()
day_index = {}
for i in xrange(0, len(day)):
	day_index[day.index[i]] = i

writer = csv.writer(file('../../data/' + target + '/features/fs_20dayproject.csv', 'wb'))
writer.writerow(['projectid', 'fs_20dayproject'])
twenty_day = {}
for i in day.index:
	twenty_day[i] = 0
	for j in xrange(-10, 11):
		if day_index[i] + j in xrange(0, len(day)):
			twenty_day[i] = twenty_day[i] + day[day_index[i] + j]
for row in np.array(projects):
	writer.writerow([row[0], twenty_day[row[-1]]])
