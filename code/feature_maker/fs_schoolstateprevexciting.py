import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/original/projects.csv')
projects = projects.sort('date_posted')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
p = np.array(projects)

exciting = set()
for row in np.array(outcomes):
	if row[1] == 't':
		exciting.add(row[0])

state_tot = {}
state_exc = {}
project_prev = {}

print 'fs_schoolstateprevexciting preprocessing'

for row in p:
	if not state_tot.has_key(row[7]):
		state_tot[row[7]] = 0
	if not state_exc.has_key(row[7]):
		state_exc[row[7]] = 0

	if state_tot[row[7]] != 0:
		project_prev[row[0]] = float(state_exc[row[7]]) / state_tot[row[7]]
	else:
		project_prev[row[0]] = 0

	state_tot[row[7]] = state_tot[row[7]] + 1
	if row[0] in exciting:
		state_exc[row[7]] = state_exc[row[7]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_schoolstateprevexciting.csv', 'wb'))
writer.writerow(['projectid', 'fs_schoolstateprevexciting'])
projects = pd.read_csv('../../data/' + target + '/projects.csv')

print 'fs_schoolstateprevexciting finishing'

for row in np.array(projects):
	if not project_prev.has_key(row[0]):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], project_prev[row[0]]])
