import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]

projects = pd.read_csv('../../data/original/projects.csv')
train_projects = pd.read_csv('../../data/training/projects.csv')
test_projects = pd.read_csv('../../data/test/projects.csv')
p = projects.sort('date_posted')

prev_project = {}
school_cnt = {}

# preprocessing
print 'fs_primaryareaprev preprocessing'
for row in np.array(p):
	if not school_cnt.has_key(row[22]):
		school_cnt[row[22]] = 0
	prev_project[row[0]] = school_cnt[row[22]]
	school_cnt[row[22]] = school_cnt[row[22]] + 1

# finishing
print 'fs_primaryareaprev finishing'
projects = pd.read_csv('../../data/' + target + '/projects.csv')
writer = csv.writer(file('../../data/' + target + '/features/fs_primaryareaprev.csv', 'wb'))
writer.writerow(['projectid', 'fs_primaryareaprev'])
for row in np.array(projects):
	writer.writerow([row[0], prev_project[row[0]]])
