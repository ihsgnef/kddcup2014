import pandas as pd
import numpy as np
import sys, csv, os

#feature_name,global,normalize,use

feature_name = 'nan'
if len(sys.argv) > 1:
	feature_name = sys.argv[1]

flist = pd.read_csv('../../data/complete_feature_list')
feature_list = np.array(flist)

if feature_name == 'last':
	row = feature_list[-1]
	feature_name = row[0]
	is_global = row[1]
	use = row[3]
	if use == True:
		if is_global == True:
			os.system('python -W ignore  ' + feature_name + '.py training')
			os.system('python -W ignore  ' + feature_name + '.py test')
		if is_global == False:
			os.system('python -W ignore  ' + feature_name + '.py original')
			os.system('python -W ignore  feature_bypasser.py ' + feature_name)
		os.system('python -W ignore  normalizer.py ' + feature_name)
	sys.exit(0)

if feature_name != 'nan':
	flist = pd.read_csv('../../data/complete_feature_list')
	feature_list = np.array(flist)
	is_global = False
	use = False
	for row in feature_list:
		if row[0] == feature_name:
			is_global = row[1]
			use = row[3]
			break
	if use == False:
		print feature_name, 'not found'
	if use == True:
		if is_global == True:
			os.system('python -W ignore  ' + feature_name + '.py training')
			os.system('python -W ignore  ' + feature_name + '.py test')
		if is_global == False:
			os.system('python -W ignore  ' + feature_name + '.py original')
			os.system('python -W ignore  feature_bypasser.py ' + feature_name)
		os.system('python -W ignore  normalizer.py ' + feature_name)
	sys.exit(0)

if feature_name == 'nan':
	for row in feature_list:
		feature_name = row[0]
		is_global = row[1]
		use = row[3]
		if use == True:
			if is_global == True:
				os.system('python -W ignore  ' + feature_name + '.py training')
				os.system('python -W ignore  ' + feature_name + '.py test')
			if is_global == False:
				os.system('python -W ignore  feature_bypasser.py ' + feature_name)
	os.system('python -W ignore  normalizer.py')
	
