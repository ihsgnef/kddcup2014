import numpy as np
import pandas as pd
import csv, sys

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/' + target + '/projects.csv')
essays = pd.read_csv('../../data/' + target + '/essays.csv')
outcomes = pd.read_csv('../../data/' + target + '/outcomes.csv')
resources = pd.read_csv('../../data/' + target + '/resources.csv')
 
