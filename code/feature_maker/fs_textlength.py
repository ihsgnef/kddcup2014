import numpy as np
import pandas as pd
import csv, sys, math

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
p = pd.read_csv('../../data/' + target + '/projects.csv')
e = pd.read_csv('../../data/' + target + '/essays.csv')
 
essay_length = {}
essays = np.array(e)
projects = np.array(p)

#projectid,teacher_acctid,title,short_description,need_statement,essay
for row in essays:
	essay_length[row[0]] = len(str(row[3])) + len(str(row[4])) + len(str(row[5]))

# print essay_length

writer = csv.writer(file('../../data/' + target + '/features/fs_textlength.csv', 'wb'))
writer.writerow(['projectid', 'fs_textlength'])

max_length = 5000

for row in projects:
	if row[0] not in essay_length:
		print 'error'
	if math.isnan(essay_length[row[0]]):
		essay_length[row[0]] = 0
	data = [row[0], essay_length[row[0]]]
	writer.writerow(data)
