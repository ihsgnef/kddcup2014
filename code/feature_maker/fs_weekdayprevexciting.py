import numpy as np
import pandas as pd
import csv, sys
import datetime

target = 'original'
if len(sys.argv) > 1:
	target = sys.argv[1]
projects = pd.read_csv('../../data/original/projects.csv')
projects = projects.sort('date_posted')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
p = np.array(projects)

exciting = set()
for row in np.array(outcomes):
	if row[1] == 't':
		exciting.add(row[0])
weekday_tot = {}
weekday_exc = {}
project_prev = {}

print 'fs_weekdayprevexciting preprocessing'

for row in p:
	day = row[-1]
	year, mon, day = int(day[:4]), int(day[5:7]), int(day[8:])
	d = datetime.datetime(year, mon, day)
	row[1] = d.weekday()

for row in p:
	if not weekday_tot.has_key(row[1]):
		weekday_tot[row[1]] = 0
	if not weekday_exc.has_key(row[1]):
		weekday_exc[row[1]] = 0

	if weekday_tot[row[1]] != 0:
		project_prev[row[0]] = float(weekday_exc[row[1]]) / weekday_tot[row[1]]
	else:
		project_prev[row[0]] = 0

	weekday_tot[row[1]] = weekday_tot[row[1]] + 1
	if row[0] in exciting:
		weekday_exc[row[1]] = weekday_exc[row[1]] + 1
	
writer = csv.writer(file('../../data/' + target + '/features/fs_weekdayprevexciting.csv', 'wb'))
writer.writerow(['projectid', 'fs_weekdayprevexciting'])
projects = pd.read_csv('../../data/' + target + '/projects.csv')

print 'fs_weekdayprevexciting finishing'

for row in np.array(projects):
	if not project_prev.has_key(row[0]):
		writer.writerow([row[0], 0])
	else:
		writer.writerow([row[0], project_prev[row[0]]])
