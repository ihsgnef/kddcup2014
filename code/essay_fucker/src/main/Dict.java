package main;

import java.util.*;
import static debug.Debug.initDebug;

;
class Dict {
	HashMap<String, Integer> mp;
	int num;

	void add(String s) {
		Integer t = mp.get(s);
		if (t != null) {
			mp.put(s, t + 1);
		} else
			mp.put(s, 1);
		num++;
	}

	int getNum(String s) {
		Integer t = mp.get(s);
		if (t == null)
			return 0;
		else
			return t;
	}

	double getRate(String s) {
		int tmp = getNum(s);
		double res = ((double) tmp) / num;
		return res;
	}
	
	void flush(HashMap<String, Integer> indexs) {
		Vector<String> tmp = new Vector<>();
		for (Map.Entry<String, Integer> i : mp.entrySet()) {
			if (i.getValue() <= 10) {
				tmp.add(i.getKey());
			}
		}
		for (String i : tmp) {
			mp.remove(i);
		}
		int num = 1;
		for (Map.Entry<String, Integer> i : mp.entrySet()) {
			indexs.put(i.getKey(), num);
			num++;
		}
	}

	Dict() {
		mp = new HashMap<>();
		num = 0;
	}

}
