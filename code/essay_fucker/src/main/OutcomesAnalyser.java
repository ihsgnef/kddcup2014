package main;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import com.csvreader.CsvReader;
import static debug.Debug.initDebug;

public class OutcomesAnalyser {
	HashMap<String, Boolean> mp;
	CsvReader in;

	void analys() throws IOException {
		in.readHeaders();
		while (in.readRecord()) {
			String id = in.get("projectid");
			String res = in.get("is_exciting");
			mp.put(id, res.equals("t"));
		}
		if (initDebug) {
			System.out.println(mp.size());
		}
	}

	Boolean get(String id) {
		return mp.get(id);
	}

	public OutcomesAnalyser(String fileName) throws IOException {
		in = new CsvReader(new FileReader(fileName));
		mp = new HashMap<>();
		analys();
		in.close();
	}
}
