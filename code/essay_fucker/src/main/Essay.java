package main;

import static debug.Debug.initDebug;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import javafx.util.Pair;

import com.sun.javafx.geom.Vec2d;

class Essay {
	Dict dict;
	String id;

	void analys(String essay, Dict dict) {
		String[] para = essay.split("\\u005cn");
		for (String i : para) {
			String[] words = i.split("[^a-zA-Z]");
			for (String j : words) {
				if (j.length() <= 3)
					continue; // 忽略过短的单词
				Stemmer stem = new Stemmer();
				for (int k = 0; k < j.length(); k++) {
					stem.add(Character.toLowerCase(j.charAt(k)));
				}
				stem.stem();
				char[] buffer = stem.getResultBuffer();
				int len = stem.getResultLength();
				String res = new String(buffer, 0, len);
				if (initDebug)
					System.out.println(res);
				dict.add(res);
			}
		}
	}

	String toLine(HashMap<String, Integer> indexs) {
		StringBuffer buffer = new StringBuffer();
		Vector<Pair<Integer, Integer>> v = new Vector<>();

		for (Map.Entry<String, Integer> i : dict.mp.entrySet()) {
			Integer index = indexs.get(i.getKey());
			if (index == null)
				continue;
			v.add(new Pair<Integer, Integer>(index, i.getValue()));
		}

		Collections.sort(v, new Comparator<Pair<Integer, Integer>>() {
			@Override
			public int compare(Pair<Integer, Integer> o1,
					Pair<Integer, Integer> o2) {
				return o1.getKey() - o2.getKey();
			}
		});
		;
		for (Pair<Integer, Integer> i : v) {
			buffer.append(i.getKey());
			buffer.append(':');
			buffer.append(i.getValue());
			buffer.append(' ');
		}
		return buffer.toString();
	}

	public String toString() {
		return null;
	}

	Essay(String id, String essay, Dict gl) {
		this.id = id;
		dict = new Dict();
		if (initDebug)
			System.out.println("start to analys data!\n");
		if (gl == null)
			analys(essay, dict);
		else
			analys(essay, gl);
	}

}
