package main;

import java.io.*;
import java.util.*;

import com.csvreader.*;

import static debug.Debug.*;

public class EssaysAnalyser {
	CsvReader in;
	PrintStream out, test, haha;
	Vector<Essay> essays;
	int essayNum;
	Dict gl;
	HashMap<String, Integer> indexs;

	void analys(OutcomesAnalyser outcomes) throws IOException {
		essays = new Vector<>();
		gl = new Dict();
		in.readHeaders();
		String[] tmps = new String[700000];
		String[] ids = new String[700000];
		Boolean[] isExcited = new Boolean[700000];
		int count = 0;
		essayNum = 0;
		while (in.readRecord()) {
			if (debug) {
				System.out.println("io :" + count);
				count++;
			}
			String id = in.get("projectid");
			String essay = in.get("essay");
			tmps[essayNum] = essay;
			ids[essayNum] = id;
			isExcited[essayNum] = outcomes.get(id);
			essayNum++;
			if (initDebug)
				break;
		}
		count = 0;
		for (int i = 0; i < essayNum; i++) {
			if (debug) {
				System.out.println("globle :" + count);
				count++;
			}
			String id = ids[i];
			String essay = tmps[i];
			try {
				new Essay(id, essay, gl);
			} catch (Exception e) {
				System.err.println("分析文件错误！");
				e.printStackTrace();
				System.exit(1);
			}
		}
		
		indexs = new HashMap<>();
		gl.flush(indexs);
		
		if (debug) {
			System.out.println(gl.num + " " + gl.mp.size());
			PrintStream os = new PrintStream(new File("dict.txt"));
			for (Map.Entry<String, Integer> i : gl.mp.entrySet()) {
				os.println(i.getKey() + " " + i.getValue());
			}
			System.exit(0);
		}
		count = 0;
		for (int i = 0; i < essayNum; i++)
			if (isExcited[i] != null) {
				if (debug) {
					System.out.println("out :" + count);
					count++;
				}
				String id = ids[i];
				String essay = tmps[i];
				try {
					if (isExcited[i])
						out.print("+1 ");
					else
						out.print("-1 ");
					Essay e = new Essay(id, essay, null);
					out.println(e.toLine(indexs));
				} catch (Exception e) {
					System.err.println("分析文件错误！");
					e.printStackTrace();
					System.exit(1);
				}
			} else {
				String id = ids[i];
				String essay = tmps[i];
				haha.println(id);
				test.print("+1 ");
				Essay e = new Essay(id, essay, null);
				test.println(e.toLine(indexs));
			}
		in.close();
	}

	public EssaysAnalyser(String fileName) throws IOException {
		in = new CsvReader(new FileReader(fileName));
		out = new PrintStream(new File("out.txt"));
		test = new PrintStream(new File("test.txt"));
		haha = new PrintStream(new File("haha.txt"));
	}
}

