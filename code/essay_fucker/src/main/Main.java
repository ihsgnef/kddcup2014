package main;

public class Main {
	EssaysAnalyser essaysAnalyser;
	OutcomesAnalyser outcomesAnalyser;

	void init() {
		try {
			outcomesAnalyser = new OutcomesAnalyser("data/outcomes.csv");
			essaysAnalyser = new EssaysAnalyser("data/essays.csv");
			essaysAnalyser.analys(outcomesAnalyser);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		System.out.println(essaysAnalyser.gl.num + "   " + essaysAnalyser.gl.mp.size());
	}

	Main() {
		init();
	}

	public static void main(String[] args) {
		new Main();
	}

}
