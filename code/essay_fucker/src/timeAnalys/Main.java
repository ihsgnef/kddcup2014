package timeAnalys;

import java.io.FileReader;
import java.util.*;

import com.csvreader.*;

public class Main {

	Vector<Project> projects;
	HashMap<String, Project> mp;
	CsvReader in;

	void init() {
		projects = new Vector<>();
		mp = new HashMap<>();
		try {
			in = new CsvReader(new FileReader(
					"/home/goblin/workspace/KDD/data/training/projects.csv"));
			in.readHeaders();
			while (in.readRecord()) {
				String id = in.get("projectid");
				String date = in.get("date_posted");
				Project p = new Project(id);
				p.date = new GDate(date);
				projects.add(p);
				mp.put(id, p);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		try {
			in = new CsvReader(new FileReader(
					"/home/goblin/workspace/KDD/data/training/outcomes.csv"));
			in.readHeaders();
			while (in.readRecord()) {
				String id = in.get("projectid");
				String res = in.get("is_exciting");
				Project p = mp.get(id);
				if (p == null) {
					System.out.println("fuck!!!");
					System.out.println(id + "   " + res);
					System.exit(0);
				}
				p.isExcited = res.equals("t");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

	}

	void sumDays() {
		int[] all = new int[8];
		int[] excited = new int[8];

		for (Project p : projects) {
			int day = p.date.getDay();
			if (p.isExcited != null) {
				all[day]++;
				if (p.isExcited)
					excited[day]++;
			}
		}
		for (int i = 1; i < 8; i++) {
			System.out.println(excited[i] + " / " + all[i] + " = "
					+ ((double) excited[i]) / all[i]);
		}
	}

	void sumNext() {
		Collections.sort(projects, new Comparator<Project>() {
			@Override
			public int compare(Project o1, Project o2) {
				return o1.date.compareTo(o2.date);
			}
		});
		int n = projects.size();
		int[] next = new int[n];
		int max = 0;
		int limit = 2;
		for (int i = 0; i < n; i++) {
			next[i] = 0;
			Project pi = projects.get(i);
			GDate gi = pi.date;
			for (int j = i + 1; j < n; j++) {
				Project pj = projects.get(j);
				GDate gj = pj.date;
				if (gj.toInt() == gi.toInt()) {
					next[i]++;
				} else
					break;
			}
			max = Math.max(max, next[i]);
		}
		int[] resAll = new int[max + 1];
		int[] resExc = new int[max + 1];
		for (int i = 0; i < n; i++) {
			resAll[next[i] / 10]++;
			if (projects.get(i).isExcited)
				resExc[next[i] / 10]++;
		}
		for (int i = 0; i <= max / 10; i++)
			System.out.println("当天post数量: " + i * 10 + "~" + (i * 10 + 10)
					+ " : " + resExc[i] + " / " + resAll[i] + " = "
					+ ((double) resExc[i]) / resAll[i]);
	}

	Main() {
		init();
		// sumDays();
		sumNext();
	}

	public static void main(String[] args) {
		new Main();
	}
}

class Project {
	String id;
	GDate date;
	Boolean isExcited;

	Project(String id) {
		this.id = id;
		date = null;
		isExcited = null;
	}

}

class GDate implements Comparable<GDate> {
	int year;
	int month;
	int date;

	GDate(String str) {
		try {
			String[] s = str.split("-");
			year = Integer.parseInt(s[0]);
			month = Integer.parseInt(s[1]);
			date = Integer.parseInt(s[2]);
		} catch (Exception e) {
			System.err.println("时间格式不对， 传进来个   " + str);
			e.printStackTrace();
		}
	}

	int getDay() {
		Date d = new Date();
		d.setYear(year);
		d.setMonth(month + 1);
		d.setDate(date);
		return d.getDay();
	}

	@Override
	public String toString() {
		return year + "-" + month + "-" + "day";
	}

	@Override
	public int compareTo(GDate o) {
		if (year != o.year)
			return year - o.year;
		if (month != o.month)
			return month - o.month;
		return date - o.date;
	}

	int toInt() {
		return (year - 1) * 365 + (month - 1) * 30 + date - 1;
	}
}