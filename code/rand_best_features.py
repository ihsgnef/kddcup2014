import pandas as pd
import numpy as np
import random as rd
import csv, sys, os

complete_feature_list = pd.read_csv('../data/complete_feature_list')
complete_set = set()
normalization = {}
is_global = {}
for row in np.array(complete_feature_list):
	complete_set.add(row[0])
	normalization[row[0]] = row[2]
	is_global[row[0]] = row[1]

feature_num = int(sys.argv[1])
max_result = 0
current_length = 0
limit_step = 100
target = '_rnd_'+str(feature_num)

def evaluate(feature_list):
	ffile = file('../data/feature_list' + target, 'wb')
	writer = csv.writer(ffile)
	writer.writerow(['feature_name','global','normalize','use'])
	for f in feature_list:
		data = [f, is_global[f], normalization[f], 'TRUE']
		writer.writerow(data)
	ffile.close()
	os.system('python -W ignore logistic_regression.py ' + target)
	os.system('python -W ignore tester.py '+target+' > tmp_result'+target+'.csv')
	this_result = open('tmp_result'+target+'.csv').readline()
	try:
		return float(this_result)
	except TypeError:
		return 0

cnt = 0
big_cnt = 0
best_result = 0
while True:
	if cnt == 0 or cnt > limit_step:
		print '**************************************'
		print big_cnt
		print '**************************************'
		sys.stdout.flush()
		feature_list = rd.sample(complete_set, feature_num)
		cnt = 0
		best_result = 0
		big_cnt = big_cnt + 1
	cnt = cnt + 1
	print cnt
	sys.stdout.flush()
	rest_features = complete_set - set(feature_list)
	rest_features_set = []
	for item in rest_features:
		rest_features_set.append(item)
	replace_index = rd.choice(xrange(len(feature_list)))
	new_feature = rd.choice(rest_features_set)
	feature_list[replace_index] = new_feature
	this_result = evaluate(feature_list)
	print 'old: ', best_result
	print 'new: ', this_result
	sys.stdout.flush()
	if this_result > best_result:
		best_result = this_result
		os.system('cp ../data/feature_list' + target + ' ../data/best_feature_list'+target+'_'+str(big_cnt))
	else:
		os.system('cp ../data/best_feature_list' + target +'_'+ str(big_cnt)+' ../data/feature_list'+target)
