import pandas as pd
import numpy as np
import csv, sys, os
from itertools import chain, combinations

def all_subsets(ss):
  return chain(*map(lambda x: combinations(ss, x), range(0, len(ss)+1)))

complete_feature_list = np.array(pd.read_csv('../data/complete_feature_list'))
max_result = 0
rr = csv.writer(file('tmp_result', 'wb'))
rr.writerow(['0'])
current_length = 0

for subset in all_subsets(complete_feature_list):
	if len(subset) > current_length:
		os.system('mv ../data/best_feature_list ../data/' + str(current_length) + '_best_feature_list')
		current_length = len(subset)
	feature_list = csv.writer(file('../data/feature_list', 'wb'))
	feature_list.writerow(['feature_name','global','normalize','use'])
	for row in subset:
		feature_list.writerow(row)
	if len(subset) < 1:
		continue
	os.system('python -W ignore logistic_regression.py silience')
	os.system('python -W ignore tester.py > tmp_result')
	this_result = open('tmp_result').readline()
	if this_result != 'id error':
		this_result = float(this_result)
	else:
		continue
	print 'pre: ', this_result
	print 'max: ', max_result
	print '*************************'
	if this_result > max_result:
		os.system('cp ../data/feature_list ../data/best_feature_list')
		max_result = this_result
	

