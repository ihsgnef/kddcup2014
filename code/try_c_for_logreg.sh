#!/bin/bash

ARRAY=(0.0001 0.001 0.01 0.1 1.0 10 100 1000)

for c_val in "${ARRAY[@]}";
do
	python logistic_regression.py $c_val silience
	python tester.py
done
