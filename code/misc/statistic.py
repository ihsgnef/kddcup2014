import numpy as np
import pandas as pd


features = {1:'projectid', 2:'teacher_acctid', 3:'schoolid', 4:'school_ncesid', 5:'school_latitude', 6:'school_longitude', 7:'school_city', 8:'school_state', 9:'school_zip', 10:'school_metro', 11:'school_district', 12:'school_county', 13:'school_charter', 14:'school_magnet', 15:'school_year_round', 16:'school_nlns', 17:'school_kipp', 18:'school_charter_ready_promise', 19:'teacher_prefix', 20:'teacher_teach_for_america', 21:'teacher_ny_teaching_fellow', 22:'primary_focus_subject', 23:'primary_focus_area', 24:'secondary_focus_subject', 25:'secondary_focus_area', 26:'resource_type', 27:'poverty_level', 28:'grade_level', 29:'fulfillment_labor_materials', 30:'total_price_excluding_optional_support', 31:'total_price_including_optional_support', 32:'students_reached', 33:'eligible_double_your_impact_match', 34:'eligible_almost_home_match', 35:'date_posted'}

print features

projects = pd.read_csv('../../data/original/projects.csv')
outcomes = pd.read_csv('../../data/original/outcomes.csv')
exciting = projects[outcomes.is_exciting == 't']

col = raw_input('choose column to calculate:')
col = features[int(col)]
p_cnt = pd.DataFrame(projects.groupby(col).size(), columns = ['all'])
e_cnt = pd.DataFrame(exciting.groupby(col).size(), columns = ['exc'])

p_cnt.to_csv('stat_projects.csv')
e_cnt.to_csv('stat_exciting.csv')

projects = pd.read_csv('stat_projects.csv')
exciting = pd.read_csv('stat_exciting.csv')

out = pd.merge(projects, exciting)
out['perc'] = out['exc'] / out['all']
out.to_csv('stat.csv', index = False)

'''
variance = {}

for i in features.keys():
	col = features[i]
	projects = pd.read_csv('data/training_set_sorted.csv')
	exciting = pd.read_csv('data/exciting_projects_sorted.csv')
	p_cnt = pd.DataFrame(projects.groupby(col).size(), columns = ['all'])
	e_cnt = pd.DataFrame(exciting.groupby(col).size(), columns = ['exc'])
	p_cnt.to_csv('stat_projects.csv')
	e_cnt.to_csv('stat_exciting.csv')
	
	projects = pd.read_csv('stat_projects.csv')
	exciting = pd.read_csv('stat_exciting.csv')
	
	out = pd.merge(projects, exciting)
	out['perc'] = out['exc'] / out['all']
	# variance[col] = out.var()['perc']
	variance[col] = max(out['perc']) - min(out['perc'])
	print col, variance[col]
'''
