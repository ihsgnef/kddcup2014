import pandas as pd
import numpy as np
import random 
import csv
import math

file_list = ['projects', 'resources', 'donations', 'essays', 'outcomes']
id_position = {'projects':0, 'resources':1, 'donations':1, 'essays':0, 'outcomes':0}

l1 = pd.read_csv('../data/training_list.csv')
l2 = pd.read_csv('../data/test_list.csv')
training_list = set(np.array(l1)[:, 0])
test_list = set(np.array(l2)[:, 0])

header = {'projects':['projectid', 'teacher_acctid', 'schoolid', 'school_ncesid', 'school_latitude', 'school_longitude', 'school_city', 'school_state', 'school_zip', 'school_metro', 'school_district', 'school_county', 'school_charter', 'school_magnet', 'school_year_round', 'school_nlns', 'school_kipp', 'school_charter_ready_promise', 'teacher_prefix', 'teacher_teach_for_america', 'teacher_ny_teaching_fellow', 'primary_focus_subject', 'primary_focus_area', 'secondary_focus_subject', 'secondary_focus_area', 'resource_type', 'poverty_level', 'grade_level', 'fulfillment_labor_materials', 'total_price_excluding_optional_support', 'total_price_including_optional_support', 'students_reached', 'eligible_double_your_impact_match', 'eligible_almost_home_match', 'date_posted'], 'resources':['resourceid', 'projectid', 'vendorid', 'vendor_name', 'project_resource_type', 'item_name', 'item_number', 'item_unit_price', 'item_quantity'], 'donations':['donationid', 'projectid', 'donor_acctid', 'donor_city', 'donor_state', 'donor_zip', 'is_teacher_acct', 'donation_timestamp', 'donation_to_project', 'donation_optional_support', 'donation_total', 'dollar_amount', 'donation_included_optional_support', 'payment_method', 'payment_included_acct_credit', 'payment_included_campaign_gift_card', 'payment_included_web_purchased_gift_card', 'payment_was_promo_matched', 'via_giving_page', 'for_honoree', 'donation_message'], 'essays':['projectid', 'teacher_acctid', 'title', 'short_description', 'need_statement', 'essay'], 'outcomes':['projectid', 'is_exciting', 'at_least_1_teacher_referred_donor', 'fully_funded', 'at_least_1_green_donation', 'great_chat', 'three_or_more_non_teacher_referred_donors', 'one_non_teacher_referred_donor_giving_100_plus', 'donation_from_thoughtful_donor', 'great_messages_proportion', 'teacher_referred_count', 'non_teacher_referred_count']}

for name in file_list:
	training_writer = csv.writer(file('../data/training/' + name + '.csv', 'wb'))
	test_writer = csv.writer(file('../data/test/' + name + '.csv', 'wb'))
	training_writer.writerow(header[name])
	test_writer.writerow(header[name])
	o = pd.read_csv('../data/original/' + name + '.csv')
	original = np.array(o)
	for row in original:
		if row[id_position[name]] in training_list:
			training_writer.writerow(row)
		if row[id_position[name]] in test_list:
			test_writer.writerow(row)
	print name + ' finished'
