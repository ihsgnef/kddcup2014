import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.svm import	LinearSVC
from sklearn.feature_extraction.text import TfidfVectorizer
import re,sys

silience = 0
pre_train_flag = 0
target = ''

if len(sys.argv) > 1:
	silience = 1
	if sys.argv[1] != 'silience':
		target = sys.argv[1]
	if len(sys.argv) > 2:
		silience = 0

if silience == 0:
	print '********** loading data **********'

o = pd.read_csv('../data/training/outcomes.csv')
outcomes = np.array(o)
l = outcomes[:, 1]
labels = np.zeros(len(l))
labels[np.where(l == 't')] = 1
exciting_index = np.where(outcomes[:, 1] == 't')[0]

flist = pd.read_csv('../data/feature_list' + target)
feature_list = np.array(flist)
if silience == 0:
	print ''
	print 'num of features:', len(np.where(feature_list[:, 3] == True)[0]), '/', len(flist)
	print ''

train_features = pd.DataFrame()
test_features = pd.DataFrame()

#feature_name,global,normalize,use
for feature in feature_list:
	feature_name = feature[0]
	if feature[3] == False:
		continue
	if feature_name[0] == '#':
		continue
	if silience == 0:
		print(feature_name),
	train_feature_file = pd.read_csv('../data/training/features/normalized/' + feature_name + '.csv')
	if silience == 0:
		print train_feature_file.shape
	if (train_feature_file.columns.size == 2):
		train_features[feature_name] = train_feature_file[feature_name]
	else:
		for i in xrange(1, train_feature_file.columns.size - 1):
			train_features[feature_name + str(i)] = train_feature_file[feature_name + str(i)]
	test_feature_file = pd.read_csv('../data/test/features/normalized/' + feature_name + '.csv')
	if (test_feature_file.columns.size == 2):
		test_features[feature_name] = test_feature_file[feature_name]
	else:
		for i in xrange(1, test_feature_file.columns.size - 1):
			test_features[feature_name + str(i)] = test_feature_file[feature_name + str(i)]

C_value = 0.01
# if len(sys.argv) > 1:
# 	C_value = float(sys.argv[1])
# 	print(C_value),


if pre_train_flag == 1:
	if silience == 0:
		print '############## pre-training ##############'
	svm = LinearSVC(C=0.01, penalty="l1", dual=False)
	train_features_new = svm.fit_transform(train_features, labels)
	test_features_new = svm.transform(test_features)
	print train_features.shape
	print train_features_new.shape

if silience == 0:
	print '############## training ##############'
if pre_train_flag == 1:
	lr = linear_model.LogisticRegression(penalty = 'l2', C = C_value, dual = False, tol = 0.000001, class_weight = {(),'auto'})
	lr.fit(train_features_new, labels)
	preds = lr.predict_proba(test_features_new)[:, 1]
else:
	lr = linear_model.LogisticRegression(penalty = 'l2', C = C_value, dual = False, tol = 0.000001)
	lr.fit(train_features, labels)
	preds = lr.predict_proba(test_features)[:, 1]

test_out = pd.read_csv('../data/test/projects.csv')
output = pd.DataFrame()
output['projectid'] = test_out['projectid']
output['is_exciting'] = preds
output.to_csv('../data/test/predictions'+target+'.csv', index = False)
