import pandas as pd
import numpy as np
import csv

print '*** projects ***'
p = pd.read_csv('../data/original/projects.csv')
p_writer = csv.writer(file('../data/original/projects.csv', 'wb'))
p_writer.writerow(list(p.columns.values))
for row in np.array(p):
	if row[-1] >= '2010-03-15':
		p_writer.writerow(row)

projects = np.array(p)
useful_projects = set(projects[:, 0])

print '*** outcomes ***'
o = pd.read_csv('../data/original/outcomes.csv')
o_header = list(o.columns.values)
o_writer = csv.writer(file('../data/original/outcomes.csv', 'wb'))
o_writer.writerow(o_header)
for row in np.array(o):
	if row[0] in useful_projects:
		o_writer.writerow(row)

print '*** essays ***'
e = pd.read_csv('../data/original/essays.csv')
e_header = list(e.columns.values)
e_writer = csv.writer(file('../data/original/essays.csv', 'wb'))
e_writer.writerow(e_header)
for row in np.array(e):
	if row[0] in useful_projects:
		e_writer.writerow(row)

print '*** resources ***'
e = pd.read_csv('../data/original/resources.csv')
e_header = list(e.columns.values)
e_writer = csv.writer(file('../data/original/resources.csv', 'wb'))
e_writer.writerow(e_header)
for row in np.array(e):
	if row[1] in useful_projects:
		e_writer.writerow(row)

print '*** donations ***'
e = pd.read_csv('../data/original/donations.csv')
e_header = list(e.columns.values)
e_writer = csv.writer(file('../data/original/donations.csv', 'wb'))
e_writer.writerow(e_header)
for row in np.array(e):
	if row[1] in useful_projects:
		e_writer.writerow(row)
